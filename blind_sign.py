from array import *
import hashlib
from random import randint

#-------------common functions

def makehash(x,y):
	a = x*y
	hash = hashlib.md5((str(a)).encode('utf-8')).hexdigest()  # <--fixa
	hash = int(hash,16)
	return hash

def make_x(a,c):
	return makehash(a,c)

def make_y(a,d,ID):
	a_ID = a ^ ID
	return makehash(a_ID, d)

def f(x,y):
	return x*y

def make_blind_array(rands,e,n,ID):
	bs = [0 for x in range(len(rands))]
	for row in range(len(rands)):
		r = rands[row][3]
		a = rands[row][0]
		c = rands[row][1]
		d = rands[row][2]
		x = make_x(a,c)
		y = make_y(a,d,ID)
		bs[row] = makeblind(r,f(x,y),e,n)
	return bs

#------------bank functions

def gen_bank_keys():
	p = 3
	q = 11
	n = p*q
	phi_n = (p - 1)*(q - 1)
	e = 3 # e & phi_n are co-prime
	d = 7 # 3*7 mod 20 = 1

	return [n,e,d]

def blindsign(data, n, d):
	data = (data ** d) % n
	return data

def rand_set(n):
	s = set()
	while(len(s) < n/2):
		s.add(randint(0,n-1))

	return s

def verify(rands,b,e,n,ID):
	test_bs = make_blind_array(rands,e,n,ID)
	i = 0
	for x in test_bs:
		if x != b[i]:
			return "Something is fishy in this pond"
		i += 1
	return "All is fine and dandy"

#--------client functions

def egcd(a, b):		#from https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Extended_Euclidean_algorithm
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def modinv(a, m):	#from https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Extended_Euclidean_algorithm
    gcd, x, y = egcd(a, m)
    if gcd != 1:
        return None  # modular inverse does not exist
    else:
        return x % m

def makecoin():
	return randint(0,256)

def makeblind(r, f, e, n):
	b = ((r ** e) * f) % n
	return b

def extract_blindsign(b,r,n):
	r_inv = modinv(r, n)
	if r_inv == None:
		print "errrror"

	return b * r_inv %n

def genrands(m,n):
	rands = [[0 for x in range(4)] for x in range(m)]
	for row in range(m):
		rands[row][0] = randint(0, n) #a
		rands[row][1] = randint(0, n) #c
		rands[row][2] = randint(0, n) #d
		r_inv = None
		r = None
		while r_inv == None:  #<----detta borde inte behovas
			r = randint(0, 256)
 			r_inv = modinv(r, m)
		rands[row][3] = r #r
	
	return rands

# ----------demo:

#setup bank
bank_key = gen_bank_keys()
n = bank_key[0]
e = bank_key[1]
d = bank_key[2]

#setup client
k = 10
ID = randint(0,256)

#withdraw k units, i.e client generates 2k B values
rands = genrands(2*k,n)
blind_arr = make_blind_array(rands,e,n, ID)

#bank wants proof of authenticity for k of the 2k B values
prove_these = rand_set(2*k)

#alice reveals the specified input numbers
rands_proof = [0 for x in range(k)]
b_proof = [0 for x in range(k)]
row = 0
for i in prove_these:
	rands_proof[row] = rands[i]
	b_proof[row] = blind_arr[i]
	row += 1

#the bank verifies the numbers
accepted = verify(rands_proof, b_proof,e,n,ID)
print accepted